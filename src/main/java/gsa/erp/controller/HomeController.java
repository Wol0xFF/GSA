package gsa.erp.controller;

import com.sun.org.apache.xerces.internal.dom.ChildNode;
import gsa.erp.model.*;
import gsa.erp.model.repository.TodoHistoryRepository;
import gsa.erp.model.repository.TodoRepository;
import gsa.erp.service.EventService;
import gsa.erp.service.TodoItemService;
import gsa.erp.service.UserService;
import javafx.animation.FillTransition;
import javafx.beans.InvalidationListener;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Duration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.io.IOException;
import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class HomeController {

    @FXML
    private VBox scrollEventsVBox;
    @FXML
    private VBox scrollTodoListVBox;

    @FXML
    private VBox scrollAssociationTodoListVBox;

    @FXML
    private VBox historyVBox;
    @Autowired
    private ApplicationContext context;
    @Autowired
    private EventService eventService;

    @Autowired
    private TodoHistoryRepository todoHistoryRepository;
    @Autowired
    private EventFormController eventFormController;

    @Autowired
    private TodoRepository todoRepository;
    @Inject
    private ViewEventController viewEventController;
    @FXML
    Button btnAddEvent;
    @Autowired
    private UserService userService;
    @Autowired
    private TodoItemService todoItemService;
    private Stage stage;

    @FXML
    private Tab todoTab;
    @FXML
    private Tab membersTab;

    @FXML
    public void initialize() {
        List<Event> events = eventService.findAllSortByStartDate();
        for(Event e : events){
            scrollEventsVBox.getChildren().add(buildEventComponent(e));
            scrollEventsVBox.getChildren().add(new javafx.scene.control.Separator());
        }
        //scrollEventsVBox.getChildren().addAll(events.stream().map(this::buildEventComponent).collect(Collectors.toList()));
        loadMembers();
        loadUserTodoList();
        loadAssociationTodoList();
        loadTodoListHistory();
    }

    private Node buildEventComponent(Event event) {
        HBox hBox = new HBox();
        hBox.setPadding(new Insets(15, 12, 15, 12));
        hBox.setSpacing(10);
        List<Node> elements = hBox.getChildren();
        elements.add(new Label(event.getStartDate().format(DateTimeFormatter.ofPattern("E d MMM u"))));
        Label nameLabel = new Label(event.getName());
        nameLabel.setFont(Font.font("Calibri", FontWeight.BOLD, 16.0));
        elements.add(nameLabel);
        Button seeEventButton = new Button("Voir");
        StackPane stackPane = new StackPane();
        stackPane.getChildren().add(seeEventButton);
        stackPane.setAlignment(Pos.CENTER_RIGHT);
        elements.add(stackPane);
        HBox.setHgrow(stackPane, Priority.ALWAYS);
        seeEventButton.setUserData(event);
        seeEventButton.setOnMouseClicked(e -> this.onViewEventButtonClicked(e, ((Event)((Button)e.getSource()).getUserData())));

        return hBox;
    }


    private void onViewEventButtonClicked(MouseEvent actionEvent, Event event) {
        try {
            this.stage = (Stage) ((Button) actionEvent.getSource()).getScene().getWindow();
            viewEventController.showEventWindow(context, stage, event);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showHomePage(ApplicationContext context) throws IOException {
        this.showHomePage(context, new Stage());
    }


    public void showHomePage(ApplicationContext context, Stage currentStage) throws IOException {
        this.context = context;
        this.stage = currentStage;
        URL url = getClass().getResource("/gsa/erp/view/home.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(url);
        fxmlLoader.setControllerFactory(context::getBean);
        Parent rootNode = fxmlLoader.load();
        currentStage.setTitle("GSA - Accueil");
        Scene scene = new Scene(rootNode);
        stage.setScene(scene);
        currentStage.show();
    }

    @FXML
    public void addEvent(ActionEvent event) throws IOException {

        UserAssociation currentUserAssociation = this.userService.getCurrentUser().getUserAssociations()
                .stream()
                .filter(userAssociation -> userAssociation.getAssociation().equals(this.userService.getCurrentAssociation()))
                .findFirst().get();
        if(currentUserAssociation.getRank() == UserAssociation.Rank.ADMIN) {
            this.stage = (Stage) ((Button) event.getSource()).getScene().getWindow();
            new EventFormController().showAddOrEditForm(context, stage);
        }
    }

    @FXML
    public void addTodoItem(ActionEvent event) throws IOException {
        this.stage = (Stage) ((Button) event.getSource()).getScene().getWindow();
        new TodoItemFormController().showAddTodoItemForm(context, stage);
    }

    @FXML
    public void addAssociationTodoItem(ActionEvent event) throws IOException {
        UserAssociation currentUserAssociation = this.userService.getCurrentUser().getUserAssociations()
                .stream()
                .filter(userAssociation -> userAssociation.getAssociation().equals(this.userService.getCurrentAssociation()))
                .findFirst().get();
        if(currentUserAssociation.getRank() == UserAssociation.Rank.ADMIN) {
            this.stage = (Stage) ((Button) event.getSource()).getScene().getWindow();
            new TodoItemFormController().showAddAssociationTodoItemForm(context, stage);
        }
    }

    @FXML
    private void loadUserTodoList() {
        List<TodoItem> userTodoList = todoItemService.getList(userService.getCurrentUser());
        for (TodoItem todoItem : userTodoList) {
            todoItem.setChecker(null);
        }
        scrollTodoListVBox.getChildren().addAll(userTodoList.stream().map(this::buildTodoListComponent).collect(Collectors.toList()));
    }

    @FXML
    private void loadAssociationTodoList() {
        List<TodoItem> associationTodoList = todoItemService.getList(userService.getCurrentAssociation());
        scrollAssociationTodoListVBox.getChildren().addAll(associationTodoList.stream().map(this::buildTodoListComponent).collect(Collectors.toList()));
    }

    @FXML
    private void loadTodoListHistory() {
        List<TodoHistory> history = this.todoHistoryRepository.findAll();
        historyVBox.getChildren().addAll(history.stream().map(todoHistory -> new Label("le " + todoHistory.getDate() + " : " + todoHistory.getAction())).collect(Collectors.toSet()));
    }

    private Node buildTodoListComponent(TodoItem todoItem) {
        HBox hBox = new HBox();
        hBox.setPadding(new Insets(15, 12, 15, 12));
        hBox.setSpacing(10);
        List<Node> elements = hBox.getChildren();
        CheckBox checkBox = new CheckBox(": ");
        checkBox.setSelected(todoItem.getDone());
        checkBox.setUserData(todoItem);
        checkBox.setOnMouseClicked(e -> this.onTodoListCheckBoxClicked(e, ((TodoItem)((CheckBox)e.getSource()).getUserData())));

        elements.add(checkBox);
        elements.add(new Label(todoItem.getTodoTask()));
        elements.add(new Label(todoItem.getChecker()));

        return hBox;
    }

    private void onTodoListCheckBoxClicked(MouseEvent actionEvent, TodoItem todoItem) {
        todoItemService.checkItem(todoItem);
    }

    @FXML
    private void loadMembers() {
        TableView<User> membersTable = new TableView<>();

        membersTab.setContent(membersTable);
        TableColumn<User, String> firstNameColumn = new TableColumn<>("Prénom");
        TableColumn<User, String> lastNameColumn = new TableColumn<>("Nom");
        TableColumn<User, String> roleColumn = new TableColumn<>("Rôle");

        firstNameColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFirstName()));
        lastNameColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getLastName()));
        roleColumn.setCellValueFactory(cellData -> {
            Association association = this.userService.getCurrentAssociation();
            return new SimpleStringProperty(cellData.getValue().getUserAssociations().stream()
                    .filter(userAsso -> userAsso.getAssociation().equals(association))
                    .findAny()
                    .get()
                    .getRank()
                    .getLabel()
            );
        });

        membersTable.getColumns().addAll(firstNameColumn, lastNameColumn, roleColumn);
        ObservableList<User> users = FXCollections.observableArrayList();
        users.addAll(userService.getCurrentAssociation().getMembers());
        membersTable.setItems(users);
    }

    private Node buildMembersComponent(User member) {
        HBox hBox = new HBox();
        hBox.setPadding(new Insets(15, 12, 15, 12));
        hBox.setSpacing(10);
        List<Node> elements = hBox.getChildren();
        elements.add(new Label(member.getFirstName()));
        elements.add(new Label(member.getLastName()));

        return hBox;
    }

}
