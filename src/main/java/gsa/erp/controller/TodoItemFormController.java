package gsa.erp.controller;

import gsa.erp.model.TodoItem;
import gsa.erp.service.TodoItemService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;

@Component
public class TodoItemFormController {

    @Autowired
    private TodoItemService todoItemService;

    @Autowired
    private ApplicationContext context;

    private Stage currentStage;

    private TodoItem currentTodoItem;

    @FXML
    boolean done;

    @FXML
    int owner;

    @FXML
    TextArea todoTask;

    @FXML
    TodoItem.Type type;

    private Stage stage;

    @FXML
    private Tab todoTab;

    @FXML
    public void initialize() {
        if (currentTodoItem != null) {
            done = currentTodoItem.getDone();
            owner = currentTodoItem.getOwner();
            todoTask.setText(currentTodoItem.getTodoTask());
            type = currentTodoItem.getType();
        }

    }

    @FXML
    public void addTodoItem(ActionEvent actionEvent) throws IOException {
        TodoItem todoItem;
        todoItem = new TodoItem();

        if (this.todoTask.getText() == null) {
            showErrorEventDialog("Todo Task", "Todo Task", "TodoTask must be a string");
        }
        todoItem.setTodoTask(todoTask.getText());
        todoItemService.addItem(todoItem);
        resetValue();
        returnToMainStage(actionEvent);
    }

    @FXML
    public void addAssociationTodoItem(ActionEvent actionEvent) throws  IOException {
        TodoItem todoItem;
        todoItem = new TodoItem();

        if (this.todoTask.getText() == null) {
            showErrorEventDialog("Todo Task", "Todo Task", "TodoTask must be a string");
        }
        todoItem.setTodoTask(todoTask.getText());
        todoItemService.addAssociationItem(todoItem);
        resetValue();
        returnToMainStage(actionEvent);
    }

    @FXML
    public void showAddTodoItemForm(ApplicationContext context, Stage currentStage, TodoItem todoItem) throws IOException {
        this.context = context;
        this.currentTodoItem = todoItem;
        URL url = getClass().getResource("/gsa/erp/view/addTodoItem.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(url);
        fxmlLoader.setControllerFactory(context::getBean);
        Parent rootNode = fxmlLoader.load();

        this.currentStage = currentStage;
        this.currentStage.setTitle("GSA - Ajout d'une tâche");
        Scene scene = new Scene(rootNode);
        currentStage.setScene(scene);
        currentStage.show();
    }

    @FXML
    public void showAddTodoItemForm(ApplicationContext context, Stage currentStage) throws IOException {
        this.showAddTodoItemForm(context, currentStage, new TodoItem());
    }

    public void showAddAssociationTodoItemForm(ApplicationContext context, Stage currentStage, TodoItem todoItem) throws IOException {
        this.context = context;
        this.currentTodoItem = todoItem;
        URL url = getClass().getResource("/gsa/erp/view/addAssociationTodoItem.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(url);
        fxmlLoader.setControllerFactory(context::getBean);
        Parent rootNode = fxmlLoader.load();

        this.currentStage = currentStage;
        this.currentStage.setTitle("GSA - Ajout d'une tâche");
        Scene scene = new Scene(rootNode);
        currentStage.setScene(scene);
        currentStage.show();
    }

    @FXML
    public void showAddAssociationTodoItemForm(ApplicationContext context, Stage currentStage) throws IOException {
        this.showAddAssociationTodoItemForm(context, currentStage, new TodoItem());
    }

    @FXML
    private void cancelAddOrEditTodoItem(ActionEvent actionEvent) throws IOException{
        resetValue();
        returnToMainStage(actionEvent);

    }

    private void showErrorEventDialog(String errorName, String errorHeader, String errorContent){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error TodoItem - " + errorName);
        alert.setHeaderText("Enter a correct " + errorHeader + "");
        alert.setContentText(errorContent);
        alert.showAndWait();
    }

    private void resetValue(){
        todoTask.setText(null);
    }

    private void returnToMainStage(ActionEvent actionEvent) throws IOException{
        Stage currentStage = (Stage) ((javafx.scene.control.Button) actionEvent.getSource()).getScene().getWindow();
        new HomeController().showHomePage(context, currentStage);
    }

    /*public void showTodoPage(ApplicationContext context, Stage currentStage) throws IOException {
        this.context = context;
        this.stage = currentStage;
        URL url = getClass().getResource("/gsa/erp/view/home.fxml");

        FXMLLoader fxmlLoader = new FXMLLoader(url);
        fxmlLoader.setControllerFactory(context::getBean);
        Parent rootNode = fxmlLoader.load();
        currentStage.setTitle("GSA - Accueil");
        Scene scene = new Scene(todoTab.getContent().getParent());
        stage.setScene(scene);
        currentStage.show();
    }*/

}
