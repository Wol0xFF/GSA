package gsa.erp.service;

import gsa.erp.model.Association;
import gsa.erp.model.TodoItem;
import gsa.erp.model.User;
import gsa.erp.model.repository.AssociationRepository;
import gsa.erp.model.repository.TodoRepository;
import gsa.erp.model.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.*;

@Service("todoItemService")
public class TodoItemService {

    @Inject
    private UserService userService;
    @Inject
    private TodoRepository todoRepository;

    @Inject
    private UserRepository userRepository;

    @Inject
    private AssociationRepository associationRepository;

    @Inject
    private TodoHistoryService todoHistoryService;

    @Transactional
    public List<TodoItem> getList(User user) {
        return todoRepository.findByOwnerAndType(user.getId(), TodoItem.Type.USER);
    }

    @Transactional
    public List<TodoItem> getList(Association association) {
        return todoRepository.findByOwnerAndType(association.getId(), TodoItem.Type.ASSOCIATION);
    }

    @Transactional
    public TodoItem getTodoItem(int id) {
        return todoRepository.findById(id);
    }

    @Transactional
    public boolean isValid(TodoItem todoItem) {
        if((todoItem.getType() == TodoItem.Type.USER &&
                userRepository.findById(todoItem.getOwner()) != null) ||
                (todoItem.getType() == TodoItem.Type.ASSOCIATION &&
                        associationRepository.findById(todoItem.getOwner()) != null)) {
            return true;
        }

        return false;
    }

    @Transactional
    public void addItem(TodoItem todoItem) {
        if(todoItem.getType() == null) {
            todoItem.setType(TodoItem.Type.USER);
        }
        todoItem.setOwner(userService.getCurrentUser().getId());
        todoItem.setDone(false);
        if(this.isValid(todoItem)) {
            todoRepository.save(todoItem);
            todoHistoryService.onNewItem(todoItem);
        } else {
            throw new IllegalArgumentException(todoItem.getType() + " not found.\n");
        }

    }

    @Transactional
    public void addAssociationItem(TodoItem todoItem) {
        if(todoItem.getType() == null) {
            todoItem.setType(TodoItem.Type.ASSOCIATION);
        }
        todoItem.setOwner(userService.getCurrentAssociation().getId());
        todoItem.setDone(false);
        if(this.isValid(todoItem)) {
            todoRepository.save(todoItem);
            todoHistoryService.onNewAssociationItem(todoItem);
        } else {
            throw new IllegalArgumentException(todoItem.getType() + " not found.\n");
        }

    }

    @Transactional
    public void deleteItem(TodoItem todoItem) {
        todoRepository.delete(todoItem);
        // todo onItemDeleted if used
    }

    @Transactional
    public void checkItem(TodoItem todoItem) {
        if(todoItem.getDone()) {
            todoItem.setDone(false);
            todoItem.setChecker(null);
            todoHistoryService.onItemUnchecked(todoItem);
        } else {
            todoItem.setDone(true);
            todoItem.setChecker(" // validée par " + userService.getCurrentUser().getFirstName() + " " +
                    userService.getCurrentUser().getLastName());
            todoHistoryService.onItemChecked(todoItem);
        }
        todoRepository.save(todoItem);
    }

    @Transactional
    public void modifyTodoTask(TodoItem todoItem, String todoTask) {
        todoItem.setTodoTask(todoTask);
        todoRepository.save(todoItem);
        // todo onItemModified if used
    }

}

