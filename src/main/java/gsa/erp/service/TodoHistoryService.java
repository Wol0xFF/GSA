package gsa.erp.service;

import gsa.erp.model.Association;
import gsa.erp.model.TodoHistory;
import gsa.erp.model.TodoItem;
import gsa.erp.model.repository.TodoHistoryRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.time.LocalDateTime;

@Service("todoHistoryService")
public class TodoHistoryService {

    @Inject
    private TodoHistoryRepository todoHistoryRepository;

    @Inject
    private UserService userService;

    @Inject
    private AssociationService associationService;

    @Transactional
    public void onNewItem(TodoItem todoItem) {
        TodoHistory th = new TodoHistory();
        th.setAction("Créé item '" + todoItem.getTodoTask() + "' pour le user " + this.userService.getUser(todoItem.getOwner()).getFullName());
        th.setTodoItem(todoItem);
        th.setUser(userService.getCurrentUser());
        th.setDate(LocalDateTime.now());
        todoHistoryRepository.save(th);
    }

    public void onNewAssociationItem(TodoItem todoItem) {
        TodoHistory th = new TodoHistory();
        th.setAction("Créé item '" + todoItem.getTodoTask() + "' pour l'association " + this.associationService.getAssociation(todoItem.getOwner()).getName());
        th.setTodoItem(todoItem);
        th.setDate(LocalDateTime.now());
        th.setUser(userService.getCurrentUser());
        todoHistoryRepository.save(th);
    }

    public void onItemUnchecked(TodoItem todoItem) {
        TodoHistory th = new TodoHistory();
        th.setAction("Unchecké item '" + todoItem.getTodoTask() + "'");
        th.setTodoItem(todoItem);
        th.setDate(LocalDateTime.now());
        th.setUser(userService.getCurrentUser());
        todoHistoryRepository.save(th);
    }

    public void onItemChecked(TodoItem todoItem) {
        TodoHistory th = new TodoHistory();
        th.setAction("Checké item '" + todoItem.getTodoTask() + "'");
        th.setTodoItem(todoItem);
        th.setUser(userService.getCurrentUser());
        th.setDate(LocalDateTime.now());
        todoHistoryRepository.save(th);
    }
}

