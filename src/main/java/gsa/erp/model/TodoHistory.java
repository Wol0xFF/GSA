package gsa.erp.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name= "todo_history")
public class TodoHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @JoinColumn(name = "todo_item")
    @ManyToOne
    private TodoItem todoItem;

    @Column(name = "action", nullable = false)
    private String action;

    @JoinColumn(name = "user")
    @ManyToOne
    private User user;

    @Column(name = "date", nullable = false)
    private LocalDateTime date;

    public int getId() {
        return id;
    }

    public TodoItem getTodoItem() {
        return todoItem;
    }

    public void setTodoItem(TodoItem todoItem) {
        this.todoItem = todoItem;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
