package gsa.erp.model.repository;

import gsa.erp.model.Event;
import gsa.erp.model.Shift;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface ShiftRepository extends JpaRepository<Shift, Integer> {
    Set<Shift> findByEvent(Event event);
}
