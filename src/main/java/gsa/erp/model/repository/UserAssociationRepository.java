package gsa.erp.model.repository;

import gsa.erp.model.User;
import gsa.erp.model.UserAssociation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAssociationRepository extends JpaRepository<UserAssociation, Integer> {
}
