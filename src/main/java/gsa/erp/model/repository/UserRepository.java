package gsa.erp.model.repository;

import gsa.erp.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    @Transactional
    User findById(int id);

    @Transactional
    List<User> findByFirstNameAndLastName(String firstName, String lastName);

}
