package gsa.erp.model.repository;

import gsa.erp.model.Affectation;
import gsa.erp.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface AffectationRepository extends JpaRepository<Affectation, Integer> {
    Set<Affectation> findByActivityEvent(Event event);
}
